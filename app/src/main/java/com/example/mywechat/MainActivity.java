package com.example.mywechat;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.View;
import android.app.FragmentManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements View.OnClickListener{

    private Fragment mTab01=new WechatFragment();
    private Fragment mTab02=new friendFragment();
    private Fragment mTab03=new contactFragment();
    private Fragment mTab04=new settingsFragment();

    private FragmentManager fm;

    LinearLayout mTabWechat;
    LinearLayout mTabFriend;
    LinearLayout mTabContact;
    LinearLayout mTabSettings;

    ImageButton mimgWechat;
    ImageButton mimgFriend;
    ImageButton mimgContact;
    ImageButton mimgSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        initFragment();
        initView();
        selectfragment(0);
        initEvent();


    }
    private void initEvent(){
        mTabWechat.setOnClickListener(this);
        mTabFriend.setOnClickListener(this);
        mTabContact.setOnClickListener(this);
        mTabSettings.setOnClickListener(this);
    }
    private void initFragment(){
        fm = getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();
    }
    private void initView(){
        mTabWechat=findViewById(R.id.id_tab_wechat);
        mTabFriend = findViewById(R.id.id_tab_friend);
        mTabContact = findViewById(R.id.id_tab_contact);
        mTabSettings = findViewById(R.id.id_tab_settings);

        mimgWechat = findViewById(R.id.id_tab_wechat_image);
        mimgFriend = findViewById(R.id.id_tab_friend_image);
        mimgContact = findViewById(R.id.id_tab_contact_image);
        mimgSettings = findViewById(R.id.id_tab_settings_image);
    }
    private void hideFragment(FragmentTransaction transaction){
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }
    private void resetImgs(){
        mimgWechat.setImageResource(R.drawable.tab_weixin_normal);
        mimgFriend.setImageResource(R.drawable.tab_find_frd_normal);
        mimgContact.setImageResource(R.drawable.tab_address_normal);
        mimgSettings.setImageResource(R.drawable.tab_settings_normal);
    }
    private void selectfragment(int i){
        FragmentTransaction transaction=fm.beginTransaction();
        hideFragment(transaction);
        switch (i){
            case 0:
                transaction.show(mTab01);
                mimgWechat.setImageResource(R.drawable.tab_weixin_pressed);
                break;
            case 1:
                transaction.show(mTab02);
                mimgFriend.setImageResource(R.drawable.tab_find_frd_pressed);
                break;
            case 2:
                transaction.show(mTab03);
                mimgContact.setImageResource(R.drawable.tab_address_pressed);
                break;
            case 3:
                transaction.show(mTab04);
                mimgSettings.setImageResource(R.drawable.tab_settings_pressed);
                break;
            default:
                break;
        }
        transaction.commit();
    }
    public void onClick(View v){
        resetImgs();
        switch (v.getId()){
            case R.id.id_tab_wechat:
                selectfragment(0);
                break;
            case R.id.id_tab_friend:
                selectfragment(1);
                break;
            case R.id.id_tab_contact:
                selectfragment(2);
                break;
            case R.id.id_tab_settings:
                selectfragment(3);
                break;

        }
    }

}
