package com.example.mywechat;

public class Peaky {
    private String name;
    private int imageid;

    public Peaky(String name , int imageid)
    {
        this.name = name;
        this.imageid = imageid;
    }
    public String getName()
    {
        return name;
    }
    public int getImageid()
    {
        return imageid;
    }
}
