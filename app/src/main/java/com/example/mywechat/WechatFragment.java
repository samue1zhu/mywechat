package com.example.mywechat;


import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class WechatFragment extends Fragment {
    private List<Peaky> peakyList = new ArrayList<>();

    public WechatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tab01,container,false);
        RecyclerView recyclerView;
        initPeakys();
////        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView = view.findViewById(R.id.recycler_view);
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        Adapter adapter = new Adapter(peakyList);
        recyclerView.setAdapter(adapter);
        return view;
    }
    private void initPeakys() {
        for (int i = 0; i < 10; i++) {
            Peaky a = new Peaky("a", R.drawable.a);
            peakyList.add(a);
            Peaky b = new Peaky("b", R.drawable.b);
            peakyList.add(b);
            Peaky c = new Peaky("c", R.drawable.c);
            peakyList.add(c);
            Peaky d = new Peaky("d", R.drawable.d);
            peakyList.add(d);
            Peaky e = new Peaky("d", R.drawable.e);
            peakyList.add(e);
            Peaky f = new Peaky("f", R.drawable.f);
            peakyList.add(f);
            Peaky g = new Peaky("g", R.drawable.h);
            peakyList.add(g);
            Peaky h = new Peaky("h", R.drawable.i);
            peakyList.add(h);
            Peaky j = new Peaky("j", R.drawable.j);
            peakyList.add(j);
        }
    }

}
